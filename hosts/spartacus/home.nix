{ inputs, outputs, config, pkgs, lib, ... }:

{
  home.username = "CHANGEME";
  home.homeDirectory = "/home/CHANGEME";
  home.stateVersion = "23.11"; # Please read the comment before changing.

  home.packages = [
  ];

  home.file = {
  };

  home.sessionVariables = {
    EDITOR = "vim";
  };

  programs.btop.enable = true;

  programs.git = {
    enable = true;
    userName  = "CHANGEME";
    userEmail = "CHANGE@ME.COM";
  };

  programs.home-manager.enable = true;
}
