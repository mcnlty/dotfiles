{ config, pkgs, inputs, outputs, ... }:

{
  imports =
    [
      ./hardware-configuration.nix
      inputs.home-manager.nixosModules.home-manager
    ];

  home-manager = {
    extraSpecialArgs = {inherit inputs outputs;};
    users = {
      "CHANGEME" = import ./home.nix;
    };
  };

  boot = {
    loader.systemd-boot.enable = true;
    loader.efi.canTouchEfiVariables = true;
    initrd.kernelModules = [ "amdgpu" ];
    initrd.luks.devices."luks-1234".device = "/dev/disk/by-uuid/1234";
  };

  networking = {
    hostName = "CHANGEME";
    networkmanager.enable = true;
    wg-quick.interfaces.wg0 = {
      address = ["0.0.0.0/0"];
      dns = ["0.0.0.0"];
      peers = [
        {
          allowedIPs = ["0.0.0.0/0"];
          endpoint = "0.0.0.0:51820";
          publicKey = "";
        }
      ];
      privateKey = "";
    };
  };

  nix.settings.experimental-features = [ "nix-command" "flakes" ];

  time.timeZone = "Europe/London";

  i18n = {
    defaultLocale = "en_AU.UTF-8";
    extraLocaleSettings = {
      LC_ADDRESS = "en_AU.UTF-8";
      LC_IDENTIFICATION = "en_AU.UTF-8";
      LC_MEASUREMENT = "en_AU.UTF-8";
      LC_MONETARY = "en_AU.UTF-8";
      LC_NAME = "en_AU.UTF-8";
      LC_NUMERIC = "en_AU.UTF-8";
      LC_PAPER = "en_AU.UTF-8";
      LC_TELEPHONE = "en_AU.UTF-8";
      LC_TIME = "en_AU.UTF-8";
    };
  };

    services.xserver = {
      xkb.layout = "us";
      xkb.variant = "";
      enable = true;
      videoDrivers = [ "amdgpu" ];
      displayManager = {
        gdm = {
          enable = true;
          wayland = true;
        };
      };
    };

  users = {
    defaultUserShell = pkgs.zsh;
    extraGroups.vboxusers.members = [ "CHANGEME" ];
    users.CHANGEME = {
      isNormalUser = true;
      description = "CHANGEME";
      extraGroups = [ "networkmanager" "wheel" ];
      packages = with pkgs; [];
      openssh.authorizedKeys.keys = [
        "ssh-ed25519 AAAA"
        "ssh-ed25519 AAAA"
      ];
    };
  };

  services.getty.autologinUser = "CHANGEME";

  nixpkgs.config.allowUnfree = true;

  environment.shells = with pkgs; [ zsh ];

  environment.systemPackages = with pkgs; [
    vim
    wget
    curl
    git
    alacritty
    librewolf-wayland
    ungoogled-chromium
    tor-browser
    vlc
    libvlc
    qbittorrent
    vscodium
    obs-studio
    libreoffice-fresh
    xdg-desktop-portal-hyprland
    xdg-desktop-portal-gtk
    xdg-desktop-portal
    swaylock
    rofi-wayland
    swww
    brightnessctl
    pamixer
    playerctl
    polkit_gnome
    grim
    slurp
    wl-clipboard
    dunst
    wlogout
    gnupg
  ];

  fonts = {
    packages = with pkgs; [ 
      nerdfonts
      noto-fonts
      noto-fonts-cjk
      noto-fonts-emoji
      liberation_ttf
      fira-code
      fira-code-symbols
      meslo-lgs-nf
      corefonts
      montserrat
      roboto
      source-code-pro
    ];

    fontconfig = {
      defaultFonts = {
        serif = [ "source-code-pro" ];
        sansSerif = [ "source-code-pro" ];
        monospace = [ "firacode" ];
      };
    };
  };

  programs = {
    hyprland.enable = true;
    thunar = {
    enable = true;
    plugins = with pkgs.xfce; [
      thunar-archive-plugin
      thunar-volman
    ];
    };
    waybar = {
      enable = true;
      package = pkgs.waybar.overrideAttrs (oldAttrs: {
        mesonFlags = oldAttrs.mesonFlags ++ [ "-Dexperimental=true" ];
      });
    };
    steam = {
      enable = true;
      remotePlay.openFirewall = true;
    };
    zsh = {
      enable = true;
      enableCompletion = true;
      syntaxHighlighting.enable = true;
      oh-my-zsh = {
        enable = true;
        plugins = [ "git" ];
        theme = "robbyrussell";
      };
    };
  };

  hardware = {
    opengl = {
      enable = true;
      driSupport = true;
      driSupport32Bit = true;
    };
  };

  services.openssh = {
    enable = true;
    settings = {
      PasswordAuthentication = false;
      KbdInteractiveAuthentication = false;
      PubkeyAuthentication = true;
      X11Forwarding = false;
    };
  };

  sound.enable = true;
  services.pipewire = {
    enable = true;
    pulse.enable = true;
    jack.enable = true;
    alsa = {
      enable = true;
      support32Bit = true;
    };
  };

  services.gvfs.enable = true;
  services.tumbler.enable = true;

  virtualisation = {
    podman.enable = true;
    docker.enable = true;
    libvirtd.enable = true;
    virtualbox = {
      host.enable = true;
      guest.enable = true;
    };
  };

  xdg = {
    autostart.enable = true;
    portal = {
      enable = true;
      extraPortals = [
        pkgs.xdg-desktop-portal
        pkgs.xdg-desktop-portal-gtk
      ];
    };
  };

  environment.sessionVariables = {
    WLR_NO_HARDWARE_CURSORS = "1";
    NIXOS_OZONE_WL = "1";
    MOZ_ENABLE_WAYLAND = "1";
    XDG_SESSION_TYPE = "wayland";
    SDL_VIDEODRIVER = "wayland";
    CLUTTER_BACKEND = "wayland";
    WLR_RENDERER = "vulkan";
    XDG_CURRENT_DESKTOP = "Hyprland";
    XDG_SESSION_DESKTOP = "Hyprland";
    GTK_USE_PORTAL = "1";
    NIXOS_XDG_OPEN_USE_PORTAL = "1";
  };

  system.stateVersion = "23.11";

}
